set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
" alternatively, pass a path where Vundle should install plugins
"let path = '~/some/path/here'
"call vundle#rc(path)

""" tunning
set synmaxcol=128
set ttyfast " u got a fast terminal

""" General
set nobackup
set nowb
set noswapfile
set hidden

""" Indenting
set autoindent
set softtabstop=2
set shiftwidth=2
set tabstop=2
set expandtab
set smarttab
""" Editing visual aids
set number
set cursorline
set showmatch
set ruler
set nofoldenable
""" Search
set incsearch
set hlsearch
set ignorecase
set smartcase
""" GUI
set background=dark
set guioptions=egmrt
set guifont=Monaco:h14
""" Nerd tree plugin
let g:NERDTreeWinPos = "right"
""" Git wrap
au FileType gitcommit set tw=72
""" CtrlP
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|\.yardoc\|public$|log\|node_modules$|gulp-bundle-assets$|tmp$',
  \ 'file': '\.so$\|\.dat$|\.DS_Store$'
  \ }

" Use The Silver Searcher https://github.com/ggreer/the_silver_searcher
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

let g:ctrlp_max_height = 20


Plugin 'gmarik/vundle'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-rails.git'
Plugin 'molokai'
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlp.vim'
Plugin 'ervandew/supertab'
Plugin 'marcweber/vim-addon-mw-utils' " Snipmate's dependency
Plugin 'tomtom/tlib_vim' " Snipmate's dependency
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'ag.vim'
Plugin 'tComment'
Plugin 'trailing-whitespace'
Plugin 'endwise.vim'
Plugin 'matchit.zip'
Plugin 'airblade/vim-gitgutter'
Plugin 'slim-template/vim-slim'
Plugin 'tpope/vim-surround'
Plugin 'gabebw/vim-spec-runner'
Plugin 'MattesGroeger/vim-bookmarks'
Plugin 'jpalardy/vim-slime'
Plugin 'elixir-lang/vim-elixir'
Plugin 'jiangmiao/auto-pairs'
Plugin 'mattn/gist-vim'
Plugin 'taglist.vim'

" slim-template/vim-slim
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

filetype plugin indent on     " required

colorscheme molokai
syntax on

" Go to tab by number
noremap <D-1> 1gt
noremap <D-2> 2gt
noremap <D-3> 3gt
noremap <D-4> 4gt
noremap <D-5> 5gt
noremap <D-6> 6gt
noremap <D-7> 7gt
noremap <D-8> 8gt
noremap <D-9> 9gt
noremap <D-0> :tablast<cr>
noremap <D-y> :TlistToggle<cr>
noremap <D-F> :NERDTreeFind<cr>

function! GetSelectedLines(type, ...) range
  let sel_save = &selection
  let &selection = "inclusive"
  let reg_save = @@

  if a:type == 'n'
    silent exe a:firstline . "," . a:lastline . "y"
  elseif a:type == 'c'
    silent exe a:1 . "," . a:2 . "y"
  else
    silent exe "normal! `<" . a:type . "`>y"
  endif

  let lines = split(@@,"\n")
  let &selection = sel_save
  let @@ = reg_save
  return lines
endfunction

function! EvalMyFile(...) range
  let full_path = expand('%:p')
  let file_ext  = expand('%:e')
  let file_handlers = {
      \   'exs'   : 'elixir',
      \   'ex'    : 'elixir',
      \   'rb'    : 'ruby',
      \   'js'    : 'node',
      \}
  let handler = file_handlers[file_ext]

  if a:0 > 0
    let selected_lines = GetSelectedLines(a:1)
    let tmpfile = tempname()
    call writefile(selected_lines, tmpfile)
    execute "!" . handler . " " . tmpfile
  else
    execute "!" . handler . " " . full_path
  endif
endfunction

vnoremap <D-r> :call EvalMyFile(visualmode(), 1)<cr>
nnoremap <D-r> :call EvalMyFile()<cr>
" noremap <D-r> :!elixir %<cr>
let Tlist_Use_Right_Window = 1

if has("gui_macvim")
  " Press Ctrl-Tab to switch between open tabs (like browser tabs) to 
  " the right side. Ctrl-Shift-Tab goes the other way.
  noremap <C-Tab> :tabnext<CR>
  noremap <C-S-Tab> :tabprev<CR>

  " Switch to specific tab numbers with Command-number
  noremap <D-1> :tabn 1<CR>
  noremap <D-2> :tabn 2<CR>
  noremap <D-3> :tabn 3<CR>
  noremap <D-4> :tabn 4<CR>
  noremap <D-5> :tabn 5<CR>
  noremap <D-6> :tabn 6<CR>
  noremap <D-7> :tabn 7<CR>
  noremap <D-8> :tabn 8<CR>
  noremap <D-9> :tabn 9<CR>
  " Command-0 goes to the last tab
  noremap <D-0> :tablast<CR>
endif

noremap <D-f> :tabn 9<CR>

autocmd! GUIEnter * set vb t_vb=

" function! Strip(input_string)
"   return substitute(a:input_string, '^\s*\(.\{-}\)\s*$', '\1', '')
" endfunction
"
" let new_var = Strip(var)

function! WordsThisLine() range
  let url = 'https://translate.google.com/\#en/es/'
  let words = []
  for linenum in range(a:firstline, a:lastline)
    let words += [(getline(linenum))]
  endfor
  let full_url = url . join(words, "\n")
  silent exec "!open '" . full_url . "'"
endfu

" http://hashrocket.com/blog/posts/8-great-vim-mappings
noremap cp yap<S-}>p
set pastetoggle=<C-z>
nnoremap Q @q
vnoremap Q :norm @q<cr>
noremap <S-l> gt
noremap <S-h> gT
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
  " bind K to grep word under cursor
  " nnoremap K :Ag! "\b<C-R><C-W>\b"<CR>
  nnoremap K :Ag! "<c-r>=expand('<cword>')"<CR>
" :nmap <leader>z :%s#\<<c-r>=expand("<cword>")<cr>\>#
  

  ":nnoremap <expr> <F8> ':%s/\<'.expand('<cword>').'\>/<&>/g<CR>'

  " nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>
endif

""" vim-bookmarks settings
let g:bookmark_save_per_working_dir = 1
let g:bookmark_manage_per_buffer = 1
let g:slime_target = "tmux"
let Tlist_Sort_Type = "name"

""" Mappings
map <D-j> :!python -m json.tool<CR>
