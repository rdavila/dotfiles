# Set git autocompletion and PS1 integration
if [ -f `brew --prefix`/etc/bash_completion ]; then
      . `brew --prefix`/etc/bash_completion
fi

GIT_PS1_SHOWDIRTYSTATE=true

if [ -f /opt/local/etc/bash_completion ]; then
    . /opt/local/etc/bash_completion
fi

# PS1='\[\033[39m\]\u@\h\[\033[00m\]:\[\033[39m\]\w\[\033[31m\]$(__git_ps1)\[\033[00m\]\$ '

function parse_git_dirty {
  [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit, working directory clean" ]] && echo "*"
}
function parse_git_stash {
  [[ $(git stash list 2> /dev/null | tail -n1) != "" ]] && echo "^"
}
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\1$(parse_git_dirty)$(parse_git_stash)/"
}
PS1='\[\e[0;33m\]${HOSTNAME}\[\e[m\] \W \[\e[0;36m\]$(parse_git_branch)\[\e[m\] $ '

PATH="/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH"

alias g='git'
alias be='bundle exec'
alias bo='bundle open'
alias mywork='g log --all --author Ruben --pretty="%h %cd  %s"'
alias checkports='sudo lsof -i -n -P | grep TCP'
alias ls='ls -laG'

export PATH=/usr/local/bin:/usr/local/hadoop/bin:/usr/local/hadoop/sbin:$PATH
export PATH=$HOME/bin:$PATH
export GIT_EDITOR='/Applications/MacVim.app/Contents/MacOS/Vim -g -f'
export EDITOR='mvim'

export HADOOP_PREFIX="/usr/local/hadoop"
export HADOOP_HOME="${HADOOP_PREFIX}"
export HADOOP_COMMON_HOME="${HADOOP_PREFIX}"
export HADOOP_CONF_DIR="${HADOOP_PREFIX}/etc/hadoop"
export HADOOP_HDFS_HOME="${HADOOP_PREFIX}"
export HADOOP_MAPRED_HOME="${HADOOP_PREFIX}"
export HADOOP_YARN_HOME="${HADOOP_PREFIX}"

export "PATH=${PATH}:${HADOOP_PREFIX}/bin:${HADOOP_PREFIX}/sbin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
