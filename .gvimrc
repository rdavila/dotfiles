if has("gui_macvim")
  macmenu &Edit.Find.Find\.\.\. key=<nop>
  macmenu &File.New\ Tab key=<nop>
  macmenu &File.Print key=<nop>
  macmenu &File.New\ Window key=<nop>
  map <D-p> :CtrlP<CR>
  map <D-t> :CtrlPTag<CR>
  map <D-n> :NERDTreeToggle<CR>
  map <D-N> &File.New\ Window key
	map <D-N> :maca newWindow:<CR>
	map <D-F> :NERDTreeFind<CR>
endif
